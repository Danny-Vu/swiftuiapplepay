//
//  SwiftUIApplePayApp.swift
//  SwiftUIApplePay
//
//  Created by Bao Vu on 12/9/23.
//

import SwiftUI

@main
struct SwiftUIApplePayApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
